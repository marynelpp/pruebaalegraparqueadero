<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstacionamientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estacionamientos', function (Blueprint $table) {
            $table->increments('id_estacionamiento');
            $table->integer('id_vehiculo')->unsigned();
            $table->foreign('id_vehiculo')->references('id_vehiculo')->on('vehiculos');
            $table->integer('celda');
            $table->integer('id_ent_sal');
            $table->integer('sensor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estacionamientos');
    }
}
