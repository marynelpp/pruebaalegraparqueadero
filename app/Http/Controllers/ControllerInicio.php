<?php

namespace EstacionamientoAlegra\Http\Controllers;

use Illuminate\Http\Request;

use EstacionamientoAlegra\Vehiculo;
Use DB;
use EstacionamientoAlegra\Estacionamiento;
use Barryvdh\DomPDF\Facade as PDF;


class ControllerInicio extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     //envio al index celdas ocupadas   
    $celdaso = DB::table('estacionamientos')->where('id_ent_sal', 1)->get();
    //envio a index el total de celdas disponibles
    $countr = DB::table('estacionamientos')->where('id_ent_sal', 1)->count();
       $cont= 20-$countr;
        $data = array('id' => $cont);  
        return view('index', compact('celdaso','data'));

    }
       public function reporte()
    {     
//consulta para el reporte
        $data = DB::table('estacionamientos')
                ->join('vehiculos', 'vehiculos.id_vehiculo', '=', 'estacionamientos.id_vehiculo')
                ->select('vehiculos.placa','estacionamientos.celda','estacionamientos.celda','estacionamientos.created_at')
                ->where('id_ent_sal', 1)
                ->get();


        return view('reporteuno', compact('data'));
    }

      public function reportedos()
    {     
        


        $count  = DB::table('estacionamientos')
                     ->select(DB::raw('count(*) as celd_count, celda'))
                     ->groupBy('celda')
                     ->limit(5)
                     ->get();
       


        return view('reportedos', compact('count'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('estacionamiento');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    //encontrar si ultima celda esta disponible
    $celdasd = DB::table('estacionamientos')->where('celda', 20)
    ->where('id_ent_sal', 0)
    ->get();
    //si no esta disponible asigno celdas restantes que serian de la 1 a 19 sino asigno celda=20 que es la ultima celda
    if (count($celdasd) > 0) {
        $celda = rand(1,19);
    } else {
         $celda=20; 
    }
    //validacion para obtener cantidad de registros si hay disponibilidad registro
    $countr = DB::table('estacionamientos')->count();

    if ($countr>20) {
       echo "No hay celdas disponibles";
    } else {
     //registro vehiculo   
    Vehiculo::create(['marca' => $request->marca, 
                'placa' => $request->placa,
    ]);
    
    $id = Vehiculo::all()->last()->id_vehiculo; //obtengo el id del vehiculo que ingreso
    $id_ent=1; //id_ent es la variable que tendra como valor 1 cuando el vehiculo entra y 0 cuando sale
    $val_sen =0; //valor del sensor 0 si esta inactivo y 1 si si.
    //con esta consulta verifico si el vehiculo q entra obstruyo la salida de otro vehiculo
     $sen = DB::table('estacionamientos') ->select('celda')
                    ->where('id_ent_sal', 0)
                    ->where('sensor', 1)
                    ->get();
                     
      if(count($sen) >= 1){ //verifico si encontro registro
        $celda = $sen[0]->celda; //asigno la celda del vehiculo q salio y este nuevo le obstruyo la salida
      }
    Estacionamiento::create([
        'id_vehiculo' => $id,
        'celda' => $celda,
        'id_ent_sal' => $id_ent,
        'sensor'=> $val_sen,
    ]);
    echo "¡Registro Exitoso!";
    }
   
    }
   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
