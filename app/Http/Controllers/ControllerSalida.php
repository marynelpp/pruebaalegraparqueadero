<?php

namespace EstacionamientoAlegra\Http\Controllers;

use Illuminate\Http\Request;
use EstacionamientoAlegra\Vehiculo;
Use DB;
use EstacionamientoAlegra\Estacionamiento;
use EstacionamientoAlegra\Factura;
use DateTime;
class ControllerSalida extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('salida');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    $id_veh=DB::table('vehiculos')->where('placa', $request->placa)->get();//SELECT para consultar el vehiculo cn la placa ingresada
    $id = $id_veh[0]->id_vehiculo;//obtengo el id_vehiculo 
   
    $veh = DB::table('estacionamientos')->where('id_vehiculo', $id)->get(); //obtengo el id vehiculo de la tabla estacionamiento
    if(count($veh) >= 1){//si existe el vehiculo en el estacionamiento cn la placa ingresada en la salida actualizo
    $id_est=$veh[0]->id_estacionamiento;
    $dt = new DateTime();
    $dt->format('Y-m-d H:i:s');
    $fec= $dt->format('Y-m-d H:i:s');
    DB::table('estacionamientos')->where('id_estacionamiento', $id_est)->update(['id_ent_sal' => 0,'updated_at'=>$fec]);
 
    }
    //codigo para la factura
     $ing_sal = DB::table('estacionamientos') ->select('created_at','updated_at')
                    ->where('id_vehiculo', $id)
                    ->where('id_ent_sal', 0)
                    ->get();

    $created_at = $ing_sal [0]->created_at; //obtengo el valor del campo created_at de entrada
    $updated_at = $ing_sal [0]->updated_at; //obtengo el valor del campo updated_at de salida
    $seg_ent = strtotime($created_at); // obtengo los segundos desde que ingreso al est
    $seg_sal = strtotime($updated_at); // obtengo los segundos desde de salida del est
    
    
    $segundos =  $seg_ent - $seg_sal; //calculo de segundos 
    $segundos = abs($segundos); //quito signo negativo

    $total = $segundos*30; //total factura 30pesos el establecido
   

     Factura::create([
        'id_vehiculo' => $id,
        'total' => $total,
        'segundos' => $segundos,
    ]);
     //conectar con api de alegra
    $fecha_actual = date("d-m-Y H:i:s"); 
    $datafac = array(
    'id' => $id,
    'total' => $total,
    'segundos' => $segundos,
    'fecha' => $fecha_actual
    );                                                                    
    $datajson = json_encode($datafac);                                                                                   
    $auth = base64_encode('perazaperozo@gmail.com:9ceb8cc7d2e6db010445');
    $ch = curl_init('https://app.alegra.com/api/v1/invoices/$id');
    $headers = array('Accept: application/json','Content-Type: application/json','Authorization: Basic ' . $auth,);                                                                      

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);                                                                     
    curl_setopt($ch, CURLOPT_POSTFIELDS, $datajson);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_exec($ch);
    curl_close($ch);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
