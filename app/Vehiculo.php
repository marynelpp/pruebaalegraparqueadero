<?php

namespace EstacionamientoAlegra;

use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{
    //
    protected $fillable = ['id_vehiculo','marca','placa'];


}
