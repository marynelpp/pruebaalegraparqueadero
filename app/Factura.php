<?php

namespace EstacionamientoAlegra;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    protected $fillable = ['id_vehiculo','total','segundos'];
}
