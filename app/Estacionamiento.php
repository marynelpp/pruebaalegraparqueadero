<?php

namespace EstacionamientoAlegra;

use Illuminate\Database\Eloquent\Model;

class Estacionamiento extends Model
{
    //
    protected $fillable = ['id_estacionamiento','id_vehiculo','celda','id_ent_sal','sensor'];
    
}
