
@extends('layouts.app')


@section('title','Index')


@section('content')

  
    <h1>Celdas Ocupadas</h1>
 <div class="row">
@foreach($celdaso as $celdao)
       <div class="col-sm">
            <div class="card" style="width: 18rem;">
                <div class="card-body">

                    <h5 class="card-title">Celda: {{$celdao->celda}}</h5>
               

                </div>
                    </div>
                        </div>

    @endforeach 
  
    @foreach($data as $dataq)
        <div class="col-sm">
            <div class="card" style="width: 25rem;">
                <div class="card-body">
               <h5 class="card-title">Total de Celdas Disponibles: {{$dataq}}</h5>
               

                </div>
                    </div>
                        </div>
      
    @endforeach
</div>
   

  <a href="create" class="btn btn-primary">Entrada</a>
  <a href="salida/create" class="btn btn-primary">Salida</a>
 <a href="reporteuno" class="btn btn-primary">Reporte Uno</a>
 <a href="reportedos" class="btn btn-primary">Reporte Dos</a>


@endsection




    
        

