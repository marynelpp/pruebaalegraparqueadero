
@extends('layouts.app')


@section('title','ReporteUNO')


@section('content')
    <h1 class="page-header">Listado vehículos que se encuentran actualmente en el parqueadero</h1>
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>Placa</th>
                <th>Celda Inicial</th>
                <th>Celda Final</th>
                <th>Tiempo Transcurrido</th>
            </tr>                            
        </thead>
        <tbody>
            @foreach($data as $datav)
            <tr>
                <td>{{ $datav->placa }}</td>
                <td>{{ $datav->celda }}</td>
                <td>{{ $datav->celda }}</td>
               <td><?php
                    $fecha_entrada = $datav->created_at; 
                    $fecha_actual = date("d-m-Y H:i:s");
                    $segundos = strtotime($fecha_actual) - strtotime($fecha_entrada); 
                    $segundos = abs($segundos);
                    $horas = floor ( $segundos / 3600 );
                    $minutes = ( ( $segundos  / 60 ) % 60 );
                    $seconds = ( $segundos  % 60 );
                    $time = $horas . ":" . $minutes . ":" . $seconds;
                ?>{{ $time }}</td>
            </tr>
            
            @endforeach
 

        </tbody>
    </table>
    <hr>
  
@endsection