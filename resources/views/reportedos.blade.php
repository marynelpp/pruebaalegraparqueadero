
@extends('layouts.app')


@section('title','ReporteDOS')


@section('content')
    <h1 class="page-header">Listado 5 celdas mas ocupadas</h1>
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>Celdas</th>
                <th>Segundos</th>
            
            </tr>                            
        </thead>
        <tbody>
            @foreach($count as $data)
            <tr>
                <td>{{ $data->celda }}</td>
                 <td><?php
                    
                   $created_at = DB::table('estacionamientos') ->select('created_at')
                    ->where('celda', $data->celda)
                    ->get();
                    foreach ($created_at as $created) {
                        $fecha_entrada = $created->created_at;
                    }
                    
                    $fecha_actual = date("d-m-Y H:i:s");
                    $segundos = strtotime($fecha_actual) - strtotime($fecha_entrada); 
                    $segundos = abs($segundos);
              
                ?>{{ $segundos }}</td>
            
            
            @endforeach
 

        </tbody>
    </table>
    <hr>
  
@endsection