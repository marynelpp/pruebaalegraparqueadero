@extends('layouts.app')


@section('title','Salida')


@section('content')
 <div class="form-group">
     <form class="form-group" method="POST" action="/salida">
            @csrf
             <div class = "form-group">
            <label for = "">Placa</label>
            <input type = "text" name="placa" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary">Salir</button>
        <a href="/" class="btn btn-primary">Volver</a>
    </form> 
@endsection